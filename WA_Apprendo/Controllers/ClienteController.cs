﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WA_Apprendo.Models;

namespace WA_Apprendo.Controllers
{
    public class ClienteController : Controller
    {

        ApprendoEntities model = new ApprendoEntities();

        // GET: Cliente
        public ActionResult MiPerfil()
        {
            return View();
        }

        public ActionResult Mantenimiento()
        {
            return View();
        }
    }
}