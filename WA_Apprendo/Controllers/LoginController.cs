﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WA_Apprendo.Models;

namespace WA_Apprendo.Controllers
{
    public class LoginController : Controller
    {
        ApprendoEntities context = new ApprendoEntities();

        // GET: Login
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string user, string pass)
        {

            Usuario usuario = new Usuario();
            try { 
             usuario = context.Usuario.AsNoTracking().First(x => x.email == user);
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login");
            }
            
            if (usuario.pwd != pass)
                return RedirectToAction("Login");

            if (usuario.tipo == "Cliente")
                return RedirectToAction("Index", "Reserva");
            else if (usuario.tipo == "Profesor")
                return RedirectToAction("MiPerfil", "Profesor");
            else
                return RedirectToAction("Dashboard", "Administrador");



            
        }
    }
}