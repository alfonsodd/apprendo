﻿$(document).ready(function () {
    $("#reserva2").hide();
    $("#reserva3").hide();

    $(".traduccionDataTables").DataTable({
        scrollX: 100,
        responsive: true,
        language: {
            "infoEmpty": "No hay filas para mostrar", "info": "_START_ paginas de _TOTAL_ filas", "lengthMenu": "Mostrar _MENU_ filas",
            "search": "Buscar"
        }
    });

    $(".traduccionDataTables2").DataTable({
        responsive: true,
        language: {
            "infoEmpty": "No hay filas para mostrar", "info": "_START_ paginas de _TOTAL_ filas", "lengthMenu": "Mostrar _MENU_ filas",
            "search": "Buscar"
        }
    });

});



/*Escoger curso*/
var curso = "";
var nivel = "";

$("#idbtnSiguienteCurso").click(function () {
    curso = $('input:radio[name=rbnCurso]:checked').val();
    nivel = $('input:radio[name=rbnNivel]:checked').val();
    if (!curso || !nivel ) {
        alert("Debe escoger un nivel y un curso antes de seguir");
    }
    else {
        $("#reserva1").hide();
        $("#reserva2").show();
    }
}); 


/*Escoger turno*/

var dia = "";
var turno = "";

$("#idBtnSiguienteTurno").click(function () {
    dia = $('input:radio[name=rbnDia]:checked').val();
    turno = $('input:radio[name=rbnTurno]:checked').val();
    if (!dia || !turno) {
        alert("Debe escoger un día y un turno antes de seguir");
    }
    else {
        $("#reserva2").hide();
        $("#reserva3").show();
    }
}); 

/*Escoger Profesor*/
var profesor = "";

$("#idBtnSiguienteProfesor").click(function () {
    profesor = $('input:radio[name=rbnProfesor]:checked').val();
    if (!profesor) {
        alert("Debe escoger un profesor para continuar");
    }
}); 



/**/






